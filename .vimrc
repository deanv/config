" Line Numbers
noremap ; l
noremap l k
noremap k j
noremap j h

au BufNewFile,BufFilePre,BufRead *.md set filetype=markdown

set number
set shortmess+=c

" size of a hard tabstop
set tabstop=4
" always uses spaces instead of tab characters
set expandtab
" size of an "indent"
set shiftwidth=4
" ctags
set tags=./tags;/
" set autochdir
set autochdir

inoremap <c-c> <ESC>

if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
          \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'lervag/vimtex'
Plug 'w0rp/ale'
Plug 'Valloric/YouCompleteMe'

call plug#end()

syntax on
filetype plugin indent on

let g:vimtex_compiler_latexmk = {'callback' : 0}

